#### Disclaimer:  
Bullet Points do not describe everything.  
Points describe content that is important to moderator,  
THERE WILL BE MORE INFORMATION THAN POINTS DESCRIBE.  


# Videos
* [Dr. Michael Eades - 'A New Hypothesis Of Obesity'](https://www.youtube.com/watch?v=pIRurLnQ8oo)
  * First half is introduction
  * Second half is effets on Midocondria from saturated fats vs polyunsaturated fats in Midocondria

# Books
* *The Big Fat Surprise: Why Butter, Meat and Cheese Belong in a Healthy Diet*
  * By Nina Teicholz
  * Author is a research journalist
  * She has examined and sited thousands of research papers from the past 100 years
  * These point to the faulty recommendations of avoiding animal products in favor of more damaging vegetable oils and soy products